# Covid-Daily-Cases - Vizianagaram, Andhra Pradesh, India



### Introduction
---

This repo downloads daily-bulletins from official website of [Department of Health, Medical & Family Welfare, Andhra Pradesh](http://hmfw.ap.gov.in/). 

Once downloaded, last-24-hrs-cases for `Vizianagaram` everyday is extracted from the pdf bulletins, and is plotted for quick visualisation. 

2 different versions of plots are available - Mobile and Desktop. 
These plots, along with the extracted data are uploaded everyday (@ 6 PM IST)  



### Usage Instructions
---

**Prerequisites -** 
Install required packages to get-started. 
```
pip3 install -r requirements.txt
```

`requirements.txt` specifies the versions used for this repo. Please feel free to install the exact versions, if newer versions show signs of incompatibility. 

**Getting-Started -** 

1. Clone the repo, and change directory into it.

    ```
    git clone https://gitlab.com/End_2_End/vzm_covid_cases.git
    cd vzm_covid_cases
    ```

2.  Run `get_covid_data.py` to download all the bulletins from [HMFW-AP](http://hmfw.ap.gov.in/) website. It starts from Jun 20, 2020 to follow similar format of bulletins.

    ```
    python3 get_covid_data.py
    ```

    Note - It creates a folder named `bulletins/` and downloads the pdf files. Also saves the plots to `assets/` folder.

3. Once step(2) is run, this step can be run, once in a day - to download current day's data and update the plots

    ```
    python3 update_covid_data.py
    ```

Updated would be saved in `assets` folder.



### Insights and Portability
---
- This repo uses `requests` to download pdfs from [HMFW-AP](http://hmfw.ap.gov.in/) website.
    - Most of the statistics are made public by state-governments. Please feel free to check out other official government websites, to see if such data is freely available.
- Uses `Tika` package for text-extraction from the PDFs.
    - [Tika](https://pypi.org/project/tika/) worked very well (upon manual inspection) for this case. Other extractors can be used, but note that this code might not work directly, due to differences in text-alignment and extraction depending on the package used.
    - This code searches explicitly for daily-cases for `Vizianagaram` , but feel free to experiment with other towns. 
    **Note** - Name is sometimes followed by a `space( )` or `2 spaces(  )`, or even starts with a `space( )` in the bulletins. Current code handles all of them.
- Uses `matplotlib.pyplot` for basic visualisations.
    - Once the data is extracted and saved, feel free to use any available plotting tools.



### Data till date
---

**Mobile-Version -**
![alt text](assets/VZM-Covid-Cases-Mobile.png "Mobile-Version")

**Desktop-Version -**
![alt text](assets/VZM-Covid-Cases.svg "Desktop-Version")

---

### License
---

[MIT License](https://gitlab.com/End_2_End/vzm_covid_cases/-/blob/master/LICENSE)
