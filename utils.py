""" Utility functions """

import os
import fitz
import time
import logging
import requests

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt


logger = logging.getLogger("Info-Logger")
logger.setLevel(logging.INFO)

logging.getLogger('matplotlib').setLevel(logging.WARNING)


def get_formatted_date(some_date):
    return '-'.join(some_date.date().isoformat().split('-')[::-1])


def download_files(download_urls, filenames, download_dir, sleep_seconds=2):
    available_files = os.listdir(download_dir)
    available_files = [download_dir + i for i in available_files]
    available_files = list(set(available_files).intersection(filenames))
    downloadable_files = list(set(filenames).difference(available_files))

    logger.info("\n")
    if len(available_files) > 0:
        logger.info("Following files already exist | Skipping download")
        _ = [logger.info(i) for i in available_files]

    download_urls = [download_urls[p] for p in [filenames.index(i) for i in
                                                downloadable_files]]
    logger.info("\n")
    logger.info("Downloading files:")
    num_files = len(download_urls)
    for i in range(num_files):
        r = requests.get(download_urls[i], allow_redirects=False)
        if r.status_code == 200:
            open(downloadable_files[i], 'wb').write(r.content)
            logger.info("[{}/{}]: Downloaded file to {}".format(
                i+1, num_files, downloadable_files[i]))
        else:
            logger.warning("URL not found: {}".format(download_urls[i]))
        time.sleep(sleep_seconds)


def get_words(f_name):
    doc = fitz.open(f_name)
    page = doc.loadPage(0)
    page_text = page.getText("text")
    words = page_text.split("\n")
    return words


def get_index(name, word_list):
    options = [name, name+" ", name+"  ", " "+name, "  "+name, " "+name+" "]
    for option in options:
        if option in word_list:
            return word_list.index(option)


def get_daily_cases(f_name):
    all_words = get_words(f_name)
    index = get_index("Vizianagaram", all_words)
    try:
        num_cases = int(all_words[index+1])
    except:
        num_cases = int(all_words[index+2])
    return num_cases


def save_plot_desktop(csv_file):
    cases_df = pd.read_csv(csv_file)
    dates = list(cases_df["Date"])
    vzm_daily_cases = list(cases_df["Daily-Cases"])
    logger.info("Plotting Desktop-version")
    plt.rcParams.update({'font.size': 22})
    fig, ax = plt.subplots(1, 1, figsize=(16, 6))
    plt.plot(dates, vzm_daily_cases, color="orange", linewidth=2)
    plt.plot(dates, [100 for i in vzm_daily_cases], color="darkgreen")
    plt.plot(dates, [500 for i in vzm_daily_cases], color="darkred")
    ax.set_xticks(ax.get_xticks()[::15])
    ax.tick_params(axis='x', rotation=60, labelsize=9)
    ax.tick_params(axis='y', labelsize=12)
    plt.text(0.85, 0.9, "Cases today [{}]: {}\nCumulative Cases: {}".format(
        dates[-1],
        vzm_daily_cases[-1], np.sum(vzm_daily_cases)), color="black",
        fontsize=14, fontweight="semibold", fontstretch="extra-condensed",
        horizontalalignment="center", transform=ax.transAxes)
    plt.grid()
    plt.title("Daily-Covid-Cases | Vizianagaram, Andhra Pradesh\n")
    fig.savefig('assets/VZM-Covid-Cases.svg', format='svg', dpi=500)


def save_plot_mobile(csv_file):
    cases_df = pd.read_csv(csv_file)
    dates = list(cases_df["Date"])
    vzm_daily_cases = list(cases_df["Daily-Cases"])
    logger.info("Plotting Mobile-version")
    plt.rcParams.update({'font.size': 9})
    fig, ax = plt.subplots(1, 1)
    plt.plot(dates, vzm_daily_cases, color="orange", linewidth=1)
    plt.plot(dates, [100 for i in vzm_daily_cases], color="darkgreen")
    plt.plot(dates, [500 for i in vzm_daily_cases], color="darkred")
    ax.set_xticks(ax.get_xticks()[::15])
    ax.tick_params(axis='x', rotation=50, labelsize=4)
    ax.tick_params(axis='y', labelsize=12)
    plt.text(0.80, 0.9, "Cases today [{}]: {}\nCumulative Cases: {}".format(
        dates[-1],
        vzm_daily_cases[-1], np.sum(vzm_daily_cases)), color="black",
        fontsize=7, fontweight="semibold", fontstretch="extra-condensed",
        horizontalalignment="center", transform=ax.transAxes)
    plt.grid()
    plt.title("Daily-Covid-Cases | Vizianagaram, Andhra Pradesh\n")
    fig.savefig('assets/VZM-Covid-Cases-Mobile.png', format='png',
                dpi=400, bbox_inches="tight")
