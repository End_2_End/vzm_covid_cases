""" Update existing Covid-date with today's data. """

import os
import utils
import logging

import pandas as pd

from datetime import datetime, timedelta

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("Info-Logger")
# logger.setLevel(logging.INFO)

# Get today's cases
logger.info("\n")
logger.info("Getting recent data")


def get_url(f_date):
    return "http://hmfw.ap.gov.in/Daily_bullettin/" + f_date + "/" + \
            f_date + "_10AM_Telugu.pdf"


def get_recent_cases():
    today = datetime.now()
    yesterday = today - timedelta(days=1)
    today_formatted = utils.get_formatted_date(today)
    yday_formatted = utils.get_formatted_date(yesterday)

    urls = [get_url(formatted_date) for formatted_date in [
        today_formatted, yday_formatted]]
    f_names = ["bulletins/" + url.split("/")[-1] for url in urls]

    download_dir = "bulletins/"
    files_before = [download_dir + i for i in os.listdir(download_dir)]
    utils.download_files(urls, f_names, download_dir)
    files_after = [download_dir + i for i in os.listdir(download_dir)]

    recent_files = list(set(files_after).difference(files_before))

    # Wait for downloading the file
    if len(recent_files) > 0:
        return [(f_name.split("/")[-1].split("_")[0],
                 utils.get_daily_cases(f_name)) for f_name in recent_files]
    else:
        logger.warning("No new files downloaded. Please try again later")
        return None


new_cases = get_recent_cases()

if new_cases:
    dates = []
    recent_cases = []
    [dates.append(i[0]) for i in new_cases]
    [recent_cases.append(i[1]) for i in new_cases]

    data = {"Date": dates, "Daily-Cases": recent_cases}
    df = pd.DataFrame(data)

    # Append new data to existing data
    logger.info("\n")
    logger.info("Updating daily-cases")
    with open("Daily-Cases.csv", "a") as f:
        df.to_csv(f, header=False, index=False)

    data = pd.read_csv("Daily-Cases.csv")
    dates = list(data["Date"])
    vzm_daily_cases = list(data["Daily-Cases"])

    # To plot desktop_version
    utils.save_plot_desktop(dates, vzm_daily_cases)

    # To plot mobile_version
    utils.save_plot_mobile(dates, vzm_daily_cases)
