""" Get Covid-data from AP - Health, Medical & Family Welfare Department """

import os
import utils
import logging

import pandas as pd

from datetime import date, timedelta, datetime


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("Info-Logger")

sdate = date(2022, 1, 1)
edate = datetime.now().date()
all_dates = pd.date_range(sdate, edate-timedelta(days=1), freq='d')

# Converting to required format
all_dates = list(all_dates)


all_dates = [utils.get_formatted_date(i) for i in all_dates]

# Download bulletins to "bulletins/" directory
# Example-
# http://hmfw.ap.gov.in/Daily_bullettin/09-10-2020/09-10-2020_10AM_Telugu.pdf


download_dir = "bulletins/"
if not os.path.exists(download_dir):
    os.mkdir(download_dir)

urls = ["http://hmfw.ap.gov.in/Daily_bullettin/" + date + "/" +
        date + "_10AM_Telugu.pdf" for date in all_dates]
f_names = [download_dir+url.split("/")[-1] for url in urls]


logger.info("\n")
logger.info("Downloading Covid bulletins: ")
utils.download_files(urls, f_names, download_dir, 2)


# Extract, save and plot data
# Extract data
logger.info("\n")
logger.info("Extracting Covid-data: ")

dates = []
vzm_daily_cases = []
for f_name in f_names:
    try:
        date_str = f_name.split("/")[-1].split("_")[0]
        dates.append(date_str)
        daily_cases = utils.get_daily_cases(f_name)
        vzm_daily_cases.append(daily_cases)
        logger.info("Date: {} | Cases: {}".format(date_str, daily_cases))
    except RuntimeError as e:
        logger.info(e)
        pass


dates = dates[:len(vzm_daily_cases)]

# Save data
data = {"Date": dates, "Daily-Cases": vzm_daily_cases}
df = pd.DataFrame(data)

save_filename = "Daily-Cases.csv"
df.to_csv(save_filename, mode='a', header=False, index=False)
logger.info("\n")
logger.info("Saved extracted data to {}".format(save_filename))
logger.info("\n")

# To plot desktop_version
utils.save_plot_desktop(save_filename)

# To plot mobile_version
utils.save_plot_mobile(save_filename)

logger.info("\n")
logger.info("Done!")
